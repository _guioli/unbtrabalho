#include <bits/stdc++.h>
#include <iostream>
#include "image.hpp"
#include "gray.hpp"
/*Aluno: Guilherme Oliveira Loiola
Matrícula: 17/00503353
Disciplina: Orientação a objetos
*/
using namespace std;

Gray::Gray()
{
comentario="";
inicio=0;
size=0;
carry=0;
}

Gray::~Gray()
{
}

string Gray::getComment()
{
	return comentario;
}

void Gray::imprimir()
{
cout << "Altura: "<<altura<< endl;
cout << "Largura: "<<largura<<endl;
cout << "Valor máximo: "<< valormax<<endl;
cout << "Tipo: P"<< tipo<< endl;
cout << getInput()<<endl;
cout << getOutput()<<endl;
}

void Gray::read()
{

	FILE *fpIN;
	fpIN= fopen(getInput().c_str(),"rt");
	if(fpIN==NULL)
	{
		cout << "Problema na leitura do arquivos\n";
		return;
	}

	char format[100];
	char possiblecomment[100];
	char dimensoes[100];
	char maximo[100];
	int P_number;
	int largura1;
	int altura1;
	int valormax1;
	fgets(format,100,fpIN);
	//Leitura do formato da imagem
	sscanf(format, "P%d", &P_number);
	//Uso do metodo set para definir o tipo na classe
	Image::setTipo(P_number);
		fgets(possiblecomment,100,fpIN);
		if(possiblecomment[0]!='#')
		{
			sscanf(possiblecomment, "%d %d",&largura1, &altura1);
			Image::setLargura(largura1);
			Image::setAltura(altura1);
		}
		else
		{
			Gray::key(possiblecomment);
			fgets(dimensoes,100,fpIN);
			sscanf(dimensoes, "%d %d",&largura1, &altura1);
			Image::setLargura(largura1);
			Image::setAltura(altura1);
		}
		//Terceira linha significativa
		fgets(maximo,100,fpIN);
		sscanf(maximo, "%d",&valormax1);
		Image::setValormax(valormax1);
	
		char umporum;
		int umporum1;
	if(P_number==5)
	{
		for(int i=0;i<largura1*altura1;i++)
		{
			if(fscanf(fpIN,"%c", &umporum)==EOF)
			{
				printf("Erro na leitura dos pixels\n");
				return;
			}
			ascii.push_back(umporum);
		}
	}
	else if(P_number==2)
	{
		for(int i=0;i<largura1*altura1;i++)
		{
			if(fscanf(fpIN,"%d", &umporum1)==EOF)
			{
				printf("Erro na leitura dos pixels\n");
				return;
			}
			decimal.push_back(umporum1);
			//printf("%d", umporum1);
		}
	}
}
void Gray::save()
{
system("clear");
	FILE *fpOUT;
	fpOUT=fopen(getOutput().c_str(),"wt");
	if(tipo==2)
	{
		fprintf(fpOUT,"P%d\n#%d %d %d\n%d %d\n%d\n", tipo, inicio,size,carry,largura,altura,valormax);
			for(auto x: decimal)
			{
			fprintf(fpOUT,"%d ",x);
			}
		fclose(fpOUT);
	}
	if(tipo==5)
	{
		fprintf(fpOUT,"P%d\n#%d %d %d\n%d %d\n%d\n", tipo, inicio,size,carry,largura,altura,valormax);
		//crip();
			for(auto x: ascii)
			{
				fprintf(fpOUT,"%c",x);
			}
		fclose(fpOUT);
	}
}
void Gray::getMensagem()
{
	FILE *fpOUT;
	fpOUT=fopen(getOutmensagem().c_str(),"wt");
	for(int i=inicio;i<(inicio+size);i++)
		fprintf(fpOUT,"%c",ascii[i]);

}

void Gray::crip()
{
	if(tipo==5)
	{
	system("clear");
	cout<<"Mensagem descriptografada é: ";
		for(int i=inicio;i<(inicio+size);i++)
		{	
				if(ascii[i]>='A' && ascii[i]<='Z')
				{
					int x= ascii[i];
					ascii[i]=ascii[i]-carry;
					if(ascii[i]<'A')
						ascii[i]='Z'-(carry-(x-'A')-1);
					printf("%c", ascii[i]);
					
					continue;		
				}		
				else if(ascii[i]>='a'&&ascii[i]<='z')
				{
					int x= ascii[i];
					ascii[i]=ascii[i]-carry;
					if(ascii[i]<'a')
						ascii[i]='z'-(carry-(x-'a')-1);
					printf("%c", ascii[i]);
					continue;
				}	
				else
				printf("%c", ascii[i]);
		}
		cout<<endl;
		
	}
}


void Gray::key(char * chave)
{
	if(chave[0]!='#')
	return;
	int inicio1;
	int size1;
	int carry1;
	sscanf(chave,"#%d %d %d", &inicio1, &size1, &carry1);
	inicio= inicio1;
	size=size1;
	carry=carry1;
}

