#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
#include "image.hpp"
#include "rgb.hpp"
/*Aluno: Guilherme Oliveira Loiola
Matrícula: 17/00503353
Disciplina: Orientação a objetos
*/
using namespace std;

Rgb::Rgb()
{
	inicio=0;
	size=0;
}

Rgb::~Rgb()
{
}

string Rgb::getComment()
{
return comentario;
}

void Rgb::read()
{

	FILE *fpIN;
	fpIN= fopen(Image::getInput().c_str(),"rt");
	if(fpIN==NULL)
	{
		cout << "Problema na leitura do arquivos\n";
		return;
	}

	char format[100],possiblecomment[100],dimensoes[100],maximo[100],c,keyword[26]="";
	int P_number,largura1,altura1,valormax1,c1;
	fgets(format,100,fpIN);
	//Leitura do formato da imagem
	sscanf(format, "P%d", &P_number);
	//Uso do metodo set para definir o tipo na classe
	Image::setTipo(P_number);
	fgets(possiblecomment,100,fpIN);
	if(possiblecomment[0]!='#')
	{
		sscanf(possiblecomment, "%d %d",&largura1, &altura1);
		Image::setLargura(largura1);
		Image::setAltura(altura1);
	}
	else
	{
		fgets(dimensoes,100,fpIN);
		sscanf(possiblecomment,"#%d %d %s",&inicio, &size, keyword);
		sscanf(dimensoes, "%d %d",&largura1, &altura1);
		Image::setLargura(largura1);
		Image::setAltura(altura1);
		this-> keyword=keyword;
	}
	//Terceira linha significativa
	fgets(maximo,100,fpIN);
	sscanf(maximo, "%d",&valormax1);
	Image::setValormax(valormax1);
	if(P_number==6)
	{
		for(int i=0;i<largura1*altura1*3;i++)
		{
			if(fscanf(fpIN,"%c", &c)==EOF)
			{
				printf("Erro na leitura dos pixels\n");
				return;
			}
			ascii.push_back(c);
		}
	}
	else if(P_number==3)
	{
		for(int i=0;i<largura1*altura1*3;i++)
		{
			if(fscanf(fpIN,"%d", &c1)==EOF)
			{
			printf("Erro na leitura dos pixels\n");
			return;
			}
		decimal.push_back(c1);
		}
	}
	fclose(fpIN);
}

void Rgb::imprimir()
{
	system("clear");
	cout << "Altura: "<<altura<< endl;
	cout << "Largura: "<<largura<<endl;
	cout << "Valor máximo: "<< valormax<<endl;
	cout << "Tipo: P"<< tipo<< endl;
	cout << "Inicio : "<< inicio<< endl;
	cout << "Size : "<< size<< endl;
	cout << "Keyword:" << keyword<< endl;
	cout << getInput()<<endl;
	cout << getOutput()<<endl;
}

void Rgb::save()
{
	FILE *fpOUT;
	fpOUT= fopen(getOutput().c_str(),"wt");
	fprintf(fpOUT,"P%d\n#Made by Guilherme\n%d %d\n%d\n", tipo,largura,altura,valormax);
	if(tipo==6)
	{
		for(auto px:ascii)
		fprintf(fpOUT,"%c", px);
	}
	else if(tipo==3)
	{
		for(int i=0;i<largura*altura;i++)
		fprintf(fpOUT,"%d", decimal[i]);
	}
}

string Rgb::getMensagem()
{
	return mensagemfinal;
}

void Rgb::crip()
{
	set<char> identify;
	map<char, int> ordem;
	vector<int> soma;
	unsigned int i;
	char cripto[size];
	char keyword[26];
	strcpy(keyword, this->keyword.c_str());
	for(i=0;i<strlen(keyword);i++)
	{
		identify.insert(keyword[i]);
		ordem[keyword[i]]=i;
	}

	for(int j=0;j<24;j++)
	{
		if(identify.count('a'+j)!=0)
		{
			continue;
		}	
		else
		{
			ordem['a'+j]=i;
			i++;
		}
	}
	for(int k=inicio;k<3*size+inicio;k+=3)
	{
		soma.push_back(ascii[k]%10+ascii[k+1]%10+ascii[k+2]%10);
	}
	for(int l=0;l<size;l++)
	{
		if(soma[l]==0)
			cripto[l]=' ';
		else
			cripto[l]='a'+ordem['a'+soma[l]-1] ;
	}
	cripto[size]='\0';
	for(auto x:cripto)
		cout<< x;
	cout << endl;
	mensagemfinal=cripto;
}



