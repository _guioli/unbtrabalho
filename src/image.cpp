#include <iostream>
#include "image.hpp"
#include "rgb.hpp"
#include "gray.hpp"
using namespace std;

Image::Image(){
	largura=1;
	altura=1;
	valormax=1;
	input="";
	output="vazio";
	tipo=0;
}
Image::~Image(){

}

void Image::setLargura(int largura){
this-> largura = largura;
}
int Image::getLargura(){
return largura;
}

void Image::setAltura(int altura){
this-> altura = altura;
}
int Image::getAltura(){
return altura;
}

void Image::setValormax(int valormax){
this-> valormax= valormax;
}
int Image::getValormax(){
return valormax;
}

void Image::setInput(string input){
this-> input = input;
}
string Image::getInput(){
return input;
}

void Image::setOutput(string output){
this-> output = output;
}
string Image::getOutput(){
return output;
}

void Image::setOutmensagem(string outmensagem){
this-> outmensagem= outmensagem;
}
string Image::getOutmensagem(){
return outmensagem;
}

void Image::setTipo(int tipo){
this-> tipo = tipo;
}
int Image::getTipo(){
return tipo;
}

void Image::imprimir(){
cout << "Altura: "<<altura<< endl;
cout << "Largura: "<<largura<<endl;
cout << "Valor máximo: "<< valormax<<endl;
cout << "Tipo: P"<< tipo<< endl;
cout << getInput()<<endl;
cout << getOutput()<<endl;
}

void Image::menu()
{
string formato="";
while(formato!="fim")
{
string input="", output="vazio";
int opcao;
system("clear");
cout << "Bem-vindo ao menu de opcoes\n";
cout << "Escreva o tipo de imagem que deseja fazer a leitura(ppm ou pgm) ou digite 'fim' para encerrar o programa\n";
cin >> formato;
if(formato=="fim")
return;
	else if(formato=="pgm")
	{
		Gray gray1;
		while(opcao!=6)
		{
			cout <<"Esse programa é capaz de realizar as seguintes operações\n1-Definir o endereço de entrada da imagem\n2-Definir o endereço de saida da imagem\n3- Imprimir os dados da imagem lida\n4-Descriptografar a imagem\n5-Salvar a imagem em outro arquivo\n6- Realizar a leitura de outra imagem\n";
			cin>> opcao;
			switch(opcao)
			{
			case 1:
				cout << "Por favor insira o input/path para a leitura da imagem\n";
				cin>> input;
				gray1.setInput(input);
				gray1.read();
				system("clear");
			break;
			case 2:
				cout <<"Por favor insira o output para salvar dados da imagem\n";
				cin>> output;
				gray1.setOutput(output);
				system("clear");
			break;
			case 3:
				system("clear");
				gray1.imprimir();
				getchar();
			break;
			case 4:
				system("clear");
				gray1.crip();
			break;
			case 5:
			if(output!="vazio"){
				gray1.save();
				system("clear");
				}
			else{
				system("clear");
				cout<< "É necessário um output para salvar a imagem\n";
			}
			break;
			case 6:
				Image imagem1;
				imagem1.menu();
			break;
		

		}
	}
}
	else if(formato=="ppm")
	{
		Rgb rgb1;
		while(opcao!=6)
		{
			cout <<"Esse programa é capaz de realizar as seguintes operações\n1-Definir o endereço de entrada da imagem\n2-Definir o endereço de saida da imagem\n3- Imprimir os dados da imagem lida\n4-Descriptografar a imagem\n5-Salvar a imagem em outro arquivo\n6- Realizar a leitura de outra imagem\n";
			cin>> opcao;
			switch(opcao)
			{
				case 1:
					cout << "Por favor insira o input/path para a leitura da imagem\n";
					cin>> input;
					rgb1.setInput(input);
					rgb1.read();
					system("clear");
				break;
				case 2:
					cout <<"Por favor insira o output para salvar dados da imagem\n";
					cin>> output;
					rgb1.setOutput(output);
					system("clear");
				break;
				case 3:
					system("clear");
					rgb1.imprimir();
					getchar();
				break;
				case 4:
					system("clear");
					cout<<"Mensagem descriptografada é: ";
					rgb1.crip();
				break;
				case 5:
				if(output!="vazio"){
					rgb1.save();
					system("clear");
					}
				else{
					system("clear");
					cout<< "É necessário um output para salvar a imagem\n";
				}
				break;
				case 6:
					Image imagem1;
					imagem1.menu();
				break;
				}
			}
		}
	}
}

