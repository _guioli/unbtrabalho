#ifndef RGB_HPP
#define RGB_HPP
#include <bits/stdc++.h>
#include "image.hpp"
/*Aluno: Guilherme Oliveira Loiola
Matrícula: 17/00503353
Disciplina: Orientação a objetos
*/
using namespace std;

class Rgb: public Image{
// Atributos
protected:
	char* comentario;
	int inicio;
	int size;
	string keyword;
	string mensagemfinal;
	vector<int> decimal;
	vector<unsigned char> ascii;

	
public:
	Rgb();
	Rgb(string tipo,int largura,int altura,int valormax, string input, string output);
	~Rgb();
	void imprimir();
	string getMensagem();
	string getComment();
	void read();
	void save();
	void crip();
};

#endif
