#ifndef GRAY_HPP
#define GRAY_HPP

#include <bits/stdc++.h>
#include "image.hpp"
using namespace std;
/*Aluno: Guilherme Oliveira Loiola
Matrícula: 17/00503353
Disciplina: Orientação a objetos
*/

class Gray : public Image
{
// Atributos
protected:
	string comentario;
	int inicio;
	int size;
	int carry;
	vector<int> decimal;
	vector<char> ascii;
public:
	Gray();
	Gray(string tipo,int largura,int altura,int valormax, string input, string output);
	~Gray();
	void imprimir();
	void terminalMensagem();
	void getMensagem();
	string getComment();
	void read();
	void save();
	void crip();
	void key(char * chave);
};
#endif
