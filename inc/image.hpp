#ifndef IMAGE_HPP
#define IMAGE_HPP
#include <iostream>

using namespace std;

class Image{
//Atributos;
protected:
	int altura;
	int largura;
	int valormax;
	//P2, P5, P3 E P6
	int tipo;
	string input="vazio";
	string output="vazio";
	string outmensagem="vazio";
	
	
		
public:
	Image();
	~Image();	

	void setLargura(int largura);
	int getLargura();

	void setAltura(int altura);
	int getAltura();

	void setValormax(int valormax);
	int getValormax();
	
	void setInput(string input);
	string getInput();

	void setOutput(string output);
	string getOutput();
	
	void setOutmensagem(string outmensagem);
	string getOutmensagem();

	void setTipo(int tipo);
	int getTipo();
	
	void menu();
	void imprimir();
	void terminalMensagem();

};
#endif
