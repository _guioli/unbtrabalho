/*Aluno: Guilherme Oliveira Loiola
Matrícula: 17/00503353
Disciplina: Orientação a objetos
*/
O programa pode ser utilizad através da escrita dos métodos diretamente na main, porém, foi feito para que o usuário tenho maior interatividade através do menu interativo.
O menu é composto por 5 opções e a escolha delas na ordem errada nao ocasionará erros como "segmentation", afinal, foi feito uma analise desses possiveis erros e como combatelos. O programa orienta o usuario atraves de escolhas intuitivas, dessa forma, se tornando uma dinamica bem simples.
Para que o programa realize a leitura basta que seja informado o input(Opção 1);
A opção 2 é necessária para salvar a imagem em outro arquivo, afinal, necessita da informação do lugar indicado para salvar.
A opção 3 imprime os dados da imagem analisada no momento.
A opção 4 faz a conversão da mensagem criptografa para a que desejamos.
A opção 5 (depende da opção 2) faz o salvamento da imagem em outro arquivo.
A opção 6 começa a leitura de outra imagem.


Obs: no encerramento do programa é necessario a digitação da string "fim" com o numero igual a quantidade de imagens lidas.
